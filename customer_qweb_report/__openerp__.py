{
    'name':'Sample Qweb Report',
    'description': 'Qweb Customer Report',
    'version':'1.0',
    'author':'Bite Pue MM Team, Myanmar',
    'data': [
	'report/report.xml',
	'view/customer_report_template.xml',
	
	
    ],
 
    'category': 'Report',
    'depends': ['website','base'],
}
